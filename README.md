# EIP_PROJECT

**Continuous Integration Tests :**

|       Codacy      |     Travis CI     |    LGTM Alerts    |  
|:-----------------:|:-----------------:|:-----------------:|
| [![Codacy Badge](https://api.codacy.com/project/badge/Grade/ad4df365f6d840b2965079e4e25d062d)](https://www.codacy.com/app/FlorianeProg/EIP_PROJECT?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=Zerekiel/EIP_PROJECT&amp;utm_campaign=Badge_Grade) | [![Build Status](https://travis-ci.org/Zerekiel/EIP_PROJECT.svg?branch=master)](https://travis-ci.org/Zerekiel/EIP_PROJECT) | [![Total alerts](https://img.shields.io/lgtm/alerts/g/Zerekiel/EIP_PROJECT.svg?logo=lgtm&logoWidth=18)](https://lgtm.com/projects/g/Zerekiel/EIP_PROJECT/alerts/) |
